module Lissajous.State where

data Wave = Sine | Square | Triangle | Saw

data State = State
   { δ :: Float  -- radian time
   , φ :: (Word,Word)  -- input frequencies
   , σ :: Float  -- radian phase shift per frame
   , ρ :: Word  -- resolution
   , α :: Float  -- amplitude
   , β :: Float  -- brightness
   , γ :: Float  -- rational grid
   , ω :: Wave
   , κ :: Bool  -- sc
   }

state :: State
state = State
   { δ = 0
   , φ = (1,1)
   , σ = 0
   , ρ = 16
   , α = 7/10
   , β = 0.2
   , γ = 9
   , ω = Sine
   , κ = False
   }

