module Main where

import Data.Fixed ( mod' )
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import Control.Concurrent
import Control.Concurrent.STM
import Lissajous.State
import Lissajous.Audio ( audioIO )

main :: IO ()
main = do
   st :: TVar State <- newTVarIO state
   _ :: ThreadId <- forkIO $ audioIO st
   playIO (InWindow "mach sim" (round $ size * 4,round $ size * 2) (0,0)) (makeColorI 0 0 0 0) fps st renderIO catchIO stepIO

fun :: Wave -> Float -> Float
fun Sine = sin
fun Square = \v' -> let v = abs $ mod' v' pi in if v < pi / 2 then 1 else -1
fun Triangle = \v' -> let v = abs $ mod' v' (2*pi) in if v < pi then 2 * (v / pi - 1) + 1 else -2 * (v / pi - 1) + 1
fun Saw = \v' -> let v = abs $ mod' v' (2*pi) in v / pi - 1

renderIO :: TVar State -> IO Picture
renderIO st = render <$> readTVarIO st

render :: State -> Picture
render st = Pictures [tx,ty,tr,curve,lx,ly]
   where

   (x,y :: Word) = φ st

-- text
   tx = translate  -size      -size $ color white $ scale 0.07 0.07 $ text $ show x
   ty = translate (-size * 2)  0    $ color white $ scale 0.07 0.07 $ text $ show y
   tr = translate (-size * 2) -size $ color white $ scale 0.07 0.07 $ text $ show (div y $ gcd x y) <> ":" <> show (div x $ gcd x y)

   xs = (* (α st * size)) . fun (ω st) . (* fromIntegral x) . (- δ st * sc) <$> init [0,2*pi / fromIntegral res..2*pi]
   ys = (* (α st * size)) . fun (ω st) . (* fromIntegral y) . (+ δ st * sc) <$> init [0,2*pi / fromIntegral res..2*pi]
   --    ^scale                           ^periodicity         ^rotation

-- xy curve
   curve = translate -size 0 $ color (makeColor 0 1 0 (β st)) $ lineLoop $ zip xs ys

-- lines
   lx = translate 0 0 $ color (makeColor 1 0.5 0 0.4) $ line $ zip [0,2 * size / fromIntegral res..] xs
   ly = translate 0 0 $ color (makeColor 0 0.5 1 0.4) $ line $ zip [0,2 * size / fromIntegral res..] ys

-- resolution coefficient
   res = ρ st ^ (2 :: Word)

-- speed coefficient (slow down more complex shapes)
   sc | κ st = 1 / fromIntegral (lcm x y)
      | otherwise = 1

catchIO :: Event -> TVar State -> IO (TVar State)
catchIO ev st = do
   atomically $ modifyTVar' st (catch ev)
   pure st

catch :: Event -> State -> State
-- catch (EventMotion c) st = st { ξ = c }
catch (EventKey k Down _ _) st
   | Char '0' <- k = state { φ = φ st , ω = ω st , σ = σ st }
   | Char 'x' <- k = st { φ = let (x,y) = φ st in (min maxBound $ succ x,y) }
   | Char 'X' <- k = st { φ = let (x,y) = φ st in (max 1 $ pred x,y) }
   | Char 'y' <- k = st { φ = let (x,y) = φ st in (x,min maxBound $ succ y) }
   | Char 'Y' <- k = st { φ = let (x,y) = φ st in (x,max 1 $ pred y) }
   | Char 's' <- k = st { σ = σ st + 0.01 }
   | Char 'S' <- k = st { σ = σ st - 0.01 }
   | Char 'r' <- k = st { ρ = min maxBound $ succ $ ρ st }
   | Char 'R' <- k = st { ρ = max 1 $ pred $ ρ st }
   | Char 'a' <- k = st { α = α st + 0.1 }
   | Char 'A' <- k = st { α = α st - 0.1 }
   | Char 'b' <- k = st { β = β st + 0.01 }
   | Char 'B' <- k = st { β = β st - 0.01 }
   | Char 'g' <- k = st { γ = succ $ γ st }
   | Char 'G' <- k = st { γ = pred $ γ st }
   | Char 'q' <- k = st { ω = Square }
   | Char 'w' <- k = st { ω = Saw }
   | Char 'e' <- k = st { ω = Sine }
   | Char 't' <- k = st { ω = Triangle }
   | Char 'k' <- k = st { κ = True }
   | Char 'K' <- k = st { κ = False }
catch _ s = s

stepIO :: Float -> TVar State -> IO (TVar State)
stepIO ti st = do
   atomically $ modifyTVar' st (step ti)
   pure st

step :: Float -> State -> State
step _ st = st { δ = δ st + σ st }

-- size of quadrant in pixels
size :: Float
size = 100

fps :: Int
fps = 30

